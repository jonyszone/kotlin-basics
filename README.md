# Kotlin Basics #

This series assumes no programming background. Everything in Kotlin has been covered in detail.  <br />
Just have a look and let us know what you think about this series. We love feedback. Cheers from jonyszone
### What is this repository for? ###

* Basic Kotlin Programming
* Version
* [Learn Kotlin Basic Programming](https://bitbucket.org/jonyszone/kotlin-basics/src/master/)

### Topics Covered In This Repo ###

- What is Kotlin?
- Hello World Program
- Variables & Data Types in Kotlin
- Operators
- Logical Operators & Shor Circuiting
- If Else Statement and Expression
- When Statement & Expression
- While & Do-While Loops
- For Loop, Ranges & String Templating
- Functions & Default Arguments
- Function Overloading & Named Arguments
- Arrays
- Classes & Objects | User Defined Data Type
- Constructors | Primary & Secondary Constructors
- Getters & Setters + LateInit
- Inheritance | OOPS
- Overriding in Inheritance Concepts
- Polymorphism, Inheritance
- Abstract Class and Abstract Methods
- Kotlin Interface | Polymorphism included
- Type Checking and Smart Casting
- Visibility Modifiers
- Object Declaration & Expressions | Singleton Pattern
- Companion Object & JVM Static | Kotlin Factory Pattern
- Data Classes | Equals & Hashcode Method
- Enum Class and Sealed Class
- Null Safety | Safe Call and Elvis Operator
- Exception Handling | Try - Catch - Finally
- Collections | List and Map
- Higher Order Functions and Function Types
- Lambdas Expressions | Higher Order Functions
- Map, Filter, ForEach | Higher Order Functions
- Extension Functions & Inline Functions
- apply, let, with, run functions | Scoped Functions
- Generics - Introduction & vararg
- Nested Class and Inner Class


### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Repo owner or admin
* Other community or team contact