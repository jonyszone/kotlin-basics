package kotlinBasics

class Types(private val helloWorld: String) {
    override fun toString(): String {
        return "Types(helloWorld='$helloWorld')"
    }
}


fun main() {
    val type = Types("Hello, World!")
    println(type)
}